package util

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
)

func Walkdir(root string) (dirs []string, files []string, err error) {
	err = filepath.WalkDir(root, func(path string, d fs.DirEntry, err error) error {
		if d.IsDir() {
			dirs = append(dirs, path)
		} else {
			files = append(files, path)
		}
		return nil
	})
	return dirs, files, err
}

func IsNotExist(d string) bool {
	_, err := os.Stat(d)
	return errors.Is(err, os.ErrNotExist)
}

func IsDuplicate(s []string) bool {
	for n1, i1 := range s {
		for _, i2 := range s[n1+1:] {
			if i1 == i2 {
				fmt.Println("文件重复:", i2)
				return true
			}
		}
	}
	return false
}
