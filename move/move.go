package main

import (
	"batch_move/util"
	"errors"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

var (
	ErrInOutNotMatch = errors.New("in 与 out 的文件行数不一致")
	ErrInNotExist    = errors.New("文件不存在")
	ErrDuplicate     = errors.New("中有文件名重复")
)

func main() {
	// 初始化获取 flag
	flag.Parse()
	fi := flag.Arg(0)
	fo := flag.Arg(1)
	do := flag.Arg(2)
	if fi == "" || fo == "" {
		fmt.Println("错误: 请传递正确的参数")
		os.Exit(1)
	}

	// 从 flag 的参数读取文件
	ri, err := os.ReadFile(fi)
	if err != nil {
		fmt.Println("错误： in.txt 不存在")
		os.Exit(1)
	}
	ro, err := os.ReadFile(fo)
	if err != nil {
		fmt.Println("错误： out.txt 不存在")
		os.Exit(1)
	}

	// 处理成切片
	i := strings.Split(string(ri), "\n")
	o := strings.Split(string(ro), "\n")

	if err := checkAll(i, o); err != nil {
		fmt.Println("错误:", err)
		os.Exit(1)
	}

	if do == "yes" {
		if err := mkdir(split(o)); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		if err := mv(i, o); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	} else {
		mkdirPrintOnly(split(o))
		mvPrintOnly(i, o)
	}
}

func checkAll(i []string, o []string) error {
	if len(i) != len(o) {
		return ErrInOutNotMatch
	}
	if util.IsDuplicate(i) {
		return fmt.Errorf("in %w", ErrDuplicate)
	}
	if util.IsDuplicate(o) {
		return fmt.Errorf("out %w", ErrDuplicate)
	}
	return nil
}

func split(f []string) (dirs []string) {
	var m = make(map[string]struct{})
	for _, i := range f {
		m[filepath.Dir(i)] = struct{}{}
	}
	for i := range m {
		dirs = append(dirs, i)
	}
	return
}

func mkdir(d []string) error {
	fmt.Println(d)
	for _, i := range d {
		fmt.Println("mkdir", i)
		if err := os.MkdirAll(i, os.ModePerm); err != nil {
			return err
		}
	}
	return nil
}

func mv(i []string, o []string) error {
	for a, b := range i {
		fmt.Println("mv", b, o[a])
		if err := os.Rename(b, o[a]); err != nil {
			return err
		}
	}
	return nil
}

func mvPrintOnly(i []string, o []string) {
	for a, b := range i {
		fmt.Println("mv", b, o[a])
	}
}

func mkdirPrintOnly(o []string) {
	for _, b := range o {
		fmt.Println("mkdir", b)
	}
}
