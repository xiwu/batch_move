package main

import (
	"batch_move/util"
	"flag"
	"fmt"
	"os"
	"strings"
)

func main() {
	// 捕获 flag
	flag.Parse()
	if len(flag.Args()) == 0 {
		fmt.Println("没有传递参数")
		os.Exit(1)
	}

	// 遍历目录, 分开存放 目录 文件, 因为输出的 in 需要 目录 放在前面
	_, files, err := util.Walkdir(flag.Arg(0))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// 将两个slice 转换成 string, 因为量不大就不用 buffer 了
	o := strings.Join(files, "\n")
	fmt.Println(o)

	if !util.IsNotExist("in.txt") {
		fmt.Println("in.txt 已经存在")
		os.Exit(1)
	}
	if err := os.WriteFile("in.txt", []byte(o), os.ModePerm); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
